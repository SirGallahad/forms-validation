export class Registry {
  public vehicle: string
  public plate: string
  public driver: string
  public contact: string
  public entryDate: string
  public exitDate?: string
  public hourlyCharge?: number

  constructor(vehicle: string, plate: string, driver: string, contact: string, entryDate: string, exitDate?: string, hourlyCharge?:number) {
    this.vehicle = vehicle
    this.plate = plate
    this.driver = driver
    this.contact = contact
    this.entryDate = entryDate
    this.exitDate = exitDate
    this.hourlyCharge = hourlyCharge
  }

}