import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl , FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router'; 
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Registry } from './model/registry.model';


@Component({
  selector: 'app-register-parking',
  templateUrl: './register-parking.component.html',
  styleUrls: ['./register-parking.component.css']
})

export class RegisterParkingComponent {

  constructor(private router: Router, private formBuilder: FormBuilder) { }

  registerForm = this.formBuilder.group({
    vehicle: ['', [ Validators.required ] ],
    plate: ['', [ Validators.required ] ],
    driver: ['', [ Validators.required ] ],
    contact: ['', [ Validators.required ] ],
  })

   allRegistry: Registry[] = []
  
  save() {
    if (this.registerForm.valid) {
      let vehicle = this.registerForm.value.vehicle
      let plate = this.registerForm.value.plate
      let driver = this.registerForm.value.driver
      let contact = this.registerForm.value.contact
      let entryDate = new Date().toLocaleString()
      let exitDate = '-'

      let registry = new Registry(vehicle, plate, driver, contact, entryDate, exitDate)
      this.allRegistry.push(registry)

      localStorage.setItem('registry-list', JSON.stringify(this.allRegistry))

      this.router.navigateByUrl('listParking')
    }
  }

  ngOnInit() {
    let list = localStorage.getItem('registry-list')
    if (list != null) {
      this.allRegistry = JSON.parse(list)
    }
  }

  goToList() {
    this.router.navigateByUrl('listParking')
  }

}