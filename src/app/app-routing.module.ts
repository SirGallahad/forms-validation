import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ListParkingComponent } from './list-parking/list-parking.component';
import { RegisterParkingComponent} from './register-parking/register-parking.component';

const routes: Routes = [
    { path: 'listParking', component: ListParkingComponent},
    { path: 'registerParking', component: RegisterParkingComponent},
    { path: '', pathMatch: 'full', redirectTo: '/listParking'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
