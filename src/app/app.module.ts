import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterParkingComponent } from './register-parking/register-parking.component';
import { ListParkingComponent } from './list-parking/list-parking.component';


@NgModule({
  declarations: [
    AppComponent,
    RegisterParkingComponent,
    ListParkingComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
