import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Registry } from '../register-parking/model/registry.model'

@Component({
  selector: 'app-list-parking',
  templateUrl: './list-parking.component.html',
  styleUrls: ['./list-parking.component.css']
})
export class ListParkingComponent {

  allRegistry: Registry[] = []

  constructor(private router: Router) { }

  goToRegister() {
    this.router.navigateByUrl('registerParking')
  }
  
  ngOnInit(): void{
    var list = localStorage.getItem('registry-list') || ""
    this.allRegistry = JSON.parse(list)
  }

  setExit(allParked: Registry) {
    allParked.exitDate = new Date().toLocaleString()
    var entryDate = new Date(allParked.entryDate)
    var exitDate = new Date
    const timeDiff = Math.abs(entryDate.getTime() - entryDate.getTime())
    const parkedTime = Math.ceil(timeDiff/(1000*60*60))
    allParked.hourlyCharge = (parkedTime*4.5)

    localStorage.setItem('registry-list', JSON.stringify(this.allRegistry))
  }
}